#include <iostream>

int main(int argc, char*argv[])
{
   
   auto const a = std::stoi(argv[1]);
   
    for( int i = 1; i <= a; ++i )
    {
        for( int j = 1; j <= a - i; ++j )
        {
            std::cout << " ";
        }
        for( int j = 1; j <= i * 2 - 1; ++j )
        {
            std::cout << "*";
        }
        std::cout << std::endl;
    }
    return 0;
}